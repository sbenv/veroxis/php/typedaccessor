<?php

declare(strict_types=1);

namespace Tests\ObjectBox;

use PHPUnit\Framework\TestCase;
use Veroxis\Typedaccessor\ObjectBox;
use Veroxis\Typedaccessor\Exceptions\MismatchedType;
use Veroxis\Typedaccessor\Exceptions\PathDoesNotExist;
use Veroxis\Typedaccessor\Exceptions\PathTooShort;

final class ObjectBoxIntsTest extends TestCase
{
    public function testSetIntSuccess(): void
    {
        $box = ObjectBox::new();
        $success = $box->setInt(['foo'], 42);
        $this->assertTrue($success);
        $this->assertSame(42, $box->asInt('foo'));
    }

    public function testSetIntFail(): void
    {
        $box = ObjectBox::new();
        $success = $box->setInt(['foo', 'bar'], 42);
        $this->assertFalse($success);
        $this->assertNull($box->asFloat('foo', 'bar'));
    }

    public function testTrySetIntSuccess(): void
    {
        $box = ObjectBox::new();
        $box->trySetInt(['foo'], 42);
        $this->assertSame(42, $box->asInt('foo'));
    }

    public function testTrySetIntFailPathDoesNotExist(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->trySetInt(['foo', 'bar'], 42);
    }

    public function testTrySetIntFailPathTooShort(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathTooShort::class);
        $box->trySetInt([], 42);
    }

    public function testAsIntAccessors(): void
    {
        $box = ObjectBox::from([
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => [],
            'object' => (object) [],
        ]);
        $this->assertNull($box->asInt('foo'));
        $this->assertNull($box->asInt('null'));
        $this->assertSame(42, $box->asInt('int'));
        $this->assertNull($box->asInt('float'));
        $this->assertNull($box->asInt('bool'));
        $this->assertNull($box->asInt('array'));
        $this->assertNull($box->asInt('object'));
        $this->assertNull($box->asInt('non_existent'));
    }

    public function testTryAsIntString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->expectException(MismatchedType::class);
        $box->tryAsInt('foo');
    }

    public function testTryAsIntNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->expectException(MismatchedType::class);
        $box->tryAsInt('foo');
    }

    public function testTryAsIntInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->assertSame(42, $box->tryAsInt('foo'));
    }

    public function testTryAsIntFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->expectException(MismatchedType::class);
        $box->tryAsInt('foo');
    }

    public function testTryAsIntBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->expectException(MismatchedType::class);
        $box->tryAsInt('foo');
    }

    public function testTryAsIntArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->expectException(MismatchedType::class);
        $box->tryAsInt('foo');
    }

    public function testTryAsIntObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->expectException(MismatchedType::class);
        $box->tryAsInt('foo');
    }

    public function testTryAsIntNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->tryAsInt('foo');
    }

    public function testIsIntString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->assertFalse($box->isInt('foo'));
    }

    public function testIsIntNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->assertFalse($box->isInt('foo'));
    }

    public function testIsIntInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->assertTrue($box->isInt('foo'));
    }

    public function testIsIntFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->assertFalse($box->isInt('foo'));
    }

    public function testIsIntBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->assertFalse($box->isInt('foo'));
    }

    public function testIsIntArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->assertFalse($box->isInt('foo'));
    }

    public function testIsIntObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->assertFalse($box->isInt('foo'));
    }

    public function testIsIntNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->assertFalse($box->isInt('foo'));
    }
}
