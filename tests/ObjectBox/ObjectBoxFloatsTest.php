<?php

declare(strict_types=1);

namespace Tests\ObjectBox;

use PHPUnit\Framework\TestCase;
use Veroxis\Typedaccessor\ObjectBox;
use Veroxis\Typedaccessor\Exceptions\MismatchedType;
use Veroxis\Typedaccessor\Exceptions\PathDoesNotExist;
use Veroxis\Typedaccessor\Exceptions\PathTooShort;

final class ObjectBoxFloatsTest extends TestCase
{
    public function testSetFloatSuccess(): void
    {
        $box = ObjectBox::new();
        $success = $box->setFloat(['foo'], 3.1415926535);
        $this->assertTrue($success);
        $this->assertSame(3.1415926535, $box->asFloat('foo'));
    }

    public function testSetFloatFail(): void
    {
        $box = ObjectBox::new();
        $success = $box->setFloat(['foo', 'bar'], 3.1415926535);
        $this->assertFalse($success);
        $this->assertNull($box->asFloat('foo', 'bar'));
    }

    public function testTrySetFloatSuccess(): void
    {
        $box = ObjectBox::new();
        $box->trySetFloat(['foo'], 3.1415926535);
        $this->assertSame(3.1415926535, $box->asFloat('foo'));
    }

    public function testTrySetFloatFailPathDoesNotExist(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->trySetFloat(['foo', 'bar'], 3.1415926535);
    }

    public function testTrySetFloatFailPathTooShort(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathTooShort::class);
        $box->trySetFloat([], 3.1415926535);
    }

    public function testAsFloatAccessors(): void
    {
        $box = ObjectBox::from([
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => [],
            'object' => (object) [],
        ]);
        $this->assertNull($box->asFloat('foo'));
        $this->assertNull($box->asFloat('null'));
        $this->assertNull($box->asFloat('int'));
        $this->assertSame(3.1415926535, $box->asFloat('float'));
        $this->assertNull($box->asFloat('bool'));
        $this->assertNull($box->asFloat('array'));
        $this->assertNull($box->asFloat('object'));
        $this->assertNull($box->asFloat('non_existent'));
    }

    public function testTryAsFloatString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->expectException(MismatchedType::class);
        $box->tryAsFloat('foo');
    }

    public function testTryAsFloatNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->expectException(MismatchedType::class);
        $box->tryAsFloat('foo');
    }

    public function testTryAsFloatInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->expectException(MismatchedType::class);
        $box->tryAsFloat('foo');
    }

    public function testTryAsFloatFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->assertSame(3.1415926535, $box->tryAsFloat('foo'));
    }

    public function testTryAsFloatBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->expectException(MismatchedType::class);
        $box->tryAsFloat('foo');
    }

    public function testTryAsFloatArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->expectException(MismatchedType::class);
        $box->tryAsFloat('foo');
    }

    public function testTryAsFloatObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->expectException(MismatchedType::class);
        $box->tryAsFloat('foo');
    }

    public function testTryAsFloatNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->tryAsFloat('foo');
    }

    public function testIsFloatString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->assertFalse($box->isFloat('foo'));
    }

    public function testIsFloatNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->assertFalse($box->isFloat('foo'));
    }

    public function testIsFloatInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->assertFalse($box->isFloat('foo'));
    }

    public function testIsFloatFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->assertTrue($box->isFloat('foo'));
    }

    public function testIsFloatBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->assertFalse($box->isFloat('foo'));
    }

    public function testIsFloatArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->assertFalse($box->isFloat('foo'));
    }

    public function testIsFloatObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->assertFalse($box->isFloat('foo'));
    }

    public function testIsFloatNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->assertFalse($box->isFloat('foo'));
    }
}
