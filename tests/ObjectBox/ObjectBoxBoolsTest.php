<?php

declare(strict_types=1);

namespace Tests\ObjectBox;

use PHPUnit\Framework\TestCase;
use Veroxis\Typedaccessor\ObjectBox;
use Veroxis\Typedaccessor\Exceptions\PathDoesNotExist;
use Veroxis\Typedaccessor\Exceptions\PathTooShort;
use Veroxis\Typedaccessor\Exceptions\MismatchedType;

final class ObjectBoxBoolsTest extends TestCase
{
    public function testSetBoolSuccess(): void
    {
        $box = ObjectBox::new();
        $success = $box->setBool(['foo'], true);
        $this->assertTrue($success);
        $this->assertTrue($box->asBool('foo'));
    }

    public function testSetBoolFail(): void
    {
        $box = ObjectBox::new();
        $success = $box->setBool(['foo', 'bar'], true);
        $this->assertFalse($success);
        $this->assertNull($box->asBool('foo', 'bar'));
    }

    public function testTrySetBoolSuccess(): void
    {
        $box = ObjectBox::new();
        $box->trySetBool(['foo'], true);
        $this->assertTrue($box->asBool('foo'));
    }

    public function testTrySetBoolFailPathDoesNotExist(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->trySetBool(['foo', 'bar'], true);
    }

    public function testTrySetBoolFailPathTooShort(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathTooShort::class);
        $box->trySetBool([], true);
    }

    public function testAsBoolAccessors(): void
    {
        $box = ObjectBox::from([
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => [],
            'object' => (object) [],
        ]);
        $this->assertNull($box->asBool('foo'));
        $this->assertNull($box->asBool('null'));
        $this->assertNull($box->asBool('int'));
        $this->assertNull($box->asBool('float'));
        $this->assertTrue($box->asBool('bool'));
        $this->assertNull($box->asBool('array'));
        $this->assertNull($box->asBool('object'));
        $this->assertNull($box->asBool('non_existent'));
    }

    public function testTryAsBoolString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->expectException(MismatchedType::class);
        $box->tryAsBool('foo');
    }

    public function testTryAsBoolNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->expectException(MismatchedType::class);
        $box->tryAsBool('foo');
    }

    public function testTryAsBoolInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->expectException(MismatchedType::class);
        $box->tryAsBool('foo');
    }

    public function testTryAsBoolFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->expectException(MismatchedType::class);
        $box->tryAsBool('foo');
    }

    public function testTryAsBoolBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->assertTrue($box->tryAsBool('foo'));
    }

    public function testTryAsBoolArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->expectException(MismatchedType::class);
        $box->tryAsBool('foo');
    }

    public function testTryAsBoolObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->expectException(MismatchedType::class);
        $box->tryAsBool('foo');
    }

    public function testTryAsBoolNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->tryAsBool('foo');
    }

    public function testIsBoolString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->assertFalse($box->isBool('foo'));
    }

    public function testIsBoolNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->assertFalse($box->isBool('foo'));
    }

    public function testIsBoolInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->assertFalse($box->isBool('foo'));
    }

    public function testIsBoolFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->assertFalse($box->isBool('foo'));
    }

    public function testIsBoolBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->assertTrue($box->isBool('foo'));
    }

    public function testIsBoolArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->assertFalse($box->isBool('foo'));
    }

    public function testIsBoolObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->assertFalse($box->isBool('foo'));
    }

    public function testIsBoolNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->assertFalse($box->isBool('foo'));
    }
}
