<?php

declare(strict_types=1);

namespace Tests\ObjectBox;

use PHPUnit\Framework\TestCase;
use Veroxis\Typedaccessor\ObjectBox;

final class ObjectBoxTest extends TestCase
{
    public function testIsNull(): void
    {
        $box = ObjectBox::from([
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => [],
            'object' => (object) [],
        ]);
        $this->assertFalse($box->isNull('foo'));
        $this->assertTrue($box->isNull('null'));
        $this->assertFalse($box->isNull('int'));
        $this->assertFalse($box->isNull('float'));
        $this->assertFalse($box->isNull('bool'));
        $this->assertFalse($box->isNull('array'));
        $this->assertFalse($box->isNull('object'));
        $this->assertFalse($box->isNull('non_existent'));
    }

    public function testKeyExists(): void
    {
        $box = ObjectBox::from([
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => ['test2' => false],
            'array2' => [false],
            'array3' => [['foobar' => 42]],
            'object' => (object) ['test2' => false],
        ]);
        $this->assertFalse($box->exists('null', 'test', 'blaa', 'blubb'));
        $this->assertFalse($box->exists('foo', 'test', 'blaa', 'blubb'));
        $this->assertFalse($box->exists('null', 'test', 'blaa', 'blubb'));
        $this->assertFalse($box->exists('int', 'test', 'blaa', 'blubb'));
        $this->assertFalse($box->exists('float', 'test', 'blaa', 'blubb'));
        $this->assertFalse($box->exists('bool', 'test', 'blaa', 'blubb'));
        $this->assertFalse($box->exists('array', 'test', 'blaa', 'blubb'));
        $this->assertFalse($box->exists('object', 'test', 'blaa', 'blubb'));
        $this->assertFalse($box->exists('array2', 1));
        $this->assertFalse($box->exists('non_existent'));
        $this->assertTrue($box->exists('null'));
        $this->assertTrue($box->exists('int'));
        $this->assertTrue($box->exists('array', 'test2'));
        $this->assertTrue($box->exists('array2', 0));
        $this->assertTrue($box->exists('array3', 0, 'foobar'));
        $this->assertTrue($box->exists('object', 'test2'));
    }

    public function testStringable(): void
    {
        $box = ObjectBox::from([
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => [],
            'object' => (object) [],
        ]);
        $json = '{"foo":"bar","null":null,"int":42,"float":3.1415926535,"bool":true,"array":[],"object":{}}';
        $this->assertJsonStringEqualsJsonString($json, $box->toJson());
    }

    public function testConstructorsWorkEqual(): void
    {
        $data = [
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => [],
            'object' => (object) [],
        ];
        $json = '{"foo":"bar","null":null,"int":42,"float":3.1415926535,"bool":true,"array":[],"object":{}}';
        $this->assertJsonStringEqualsJsonString(
            $json,
            json_encode($data, JSON_THROW_ON_ERROR),
            'input json is not synchronized with input data'
        );
        $boxA = ObjectBox::from($data);
        $boxB = ObjectBox::from((object) $data);
        $boxC = ObjectBox::fromJson($json);
        $this->assertJsonStringEqualsJsonString($json, $boxA->toJson());
        $this->assertJsonStringEqualsJsonString($json, $boxB->toJson());
        $this->assertJsonStringEqualsJsonString($json, $boxC->toJson());
        $this->assertJsonStringEqualsJsonString($boxA->toJson(), $boxC->toJson());
        $this->assertJsonStringEqualsJsonString($boxB->toJson(), $boxC->toJson());
        $this->assertJsonStringEqualsJsonString($boxB->toJson(), $boxA->toJson());
    }

    public function testUnwrap(): void
    {
        $boxA = ObjectBox::from([
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => [],
            'object' => (object) [],
        ]);
        $boxB = ObjectBox::from($boxA->unwrap());
        $this->assertJsonStringEqualsJsonString($boxA->toJson(), $boxB->toJson());
    }

    public function testDelete(): void
    {
        $box = ObjectBox::from((object) [
            'foo' => [
                'bar' => 42
            ]
        ]);
        $this->assertJsonStringEqualsJsonString('{"foo":{"bar":42}}', $box->toJson());
        $this->assertSame(42, $box->asInt('foo', 'bar'));
        $this->assertTrue($box->delete('foo', 'bar'));
        $this->assertJsonStringEqualsJsonString('{"foo":[]}', $box->toJson());
        $this->assertNull($box->asInt('foo', 'bar'));
        $this->assertFalse($box->delete('foo', 'bar'));
        $this->assertTrue($box->delete('foo'));
        $this->assertJsonStringEqualsJsonString('{}', $box->toJson());
    }
}
