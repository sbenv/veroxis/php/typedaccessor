<?php

declare(strict_types=1);

namespace Tests\ObjectBox;

use PHPUnit\Framework\TestCase;
use Veroxis\Typedaccessor\ObjectBox;
use Veroxis\Typedaccessor\Exceptions\PathDoesNotExist;
use Veroxis\Typedaccessor\Exceptions\PathTooShort;
use Veroxis\Typedaccessor\Exceptions\MismatchedType;

final class ObjectBoxObjectsTest extends TestCase
{
    public function testSetObjectSuccess(): void
    {
        $box = ObjectBox::new();
        $success = $box->setObject(['foo'], (object) []);
        $this->assertTrue($success);
        $this->assertEqualsCanonicalizing((object) [], $box->asObject('foo'));
        $success = $box->setObject(['foo', 'bar'], (object) []);
        $this->assertTrue($success);
        $this->assertEqualsCanonicalizing((object) [], $box->asObject('foo', 'bar'));
        $success = $box->setObject(['foo', 'bar', 'baz'], (object) []);
        $this->assertTrue($success);
        $this->assertEqualsCanonicalizing((object) [], $box->asObject('foo', 'bar', 'baz'));
        $this->assertEqualsCanonicalizing(
            (object) ['foo' => (object) ['bar' => (object) ['baz' => (object) []]]],
            $box->asObject()
        );
    }

    public function testSetObjectFail(): void
    {
        $box = ObjectBox::new();
        $success = $box->setObject(['foo', 'bar'], (object) []);
        $this->assertFalse($success);
        $this->assertNull($box->asObject('foo', 'bar'));
    }

    public function testTrySetObjectSuccess(): void
    {
        $box = ObjectBox::new();
        $box->trySetObject(['foo'], (object) []);
        $this->assertEqualsCanonicalizing((object) [], $box->asObject('foo'));
    }

    public function testTrySetObjectFailPathDoesNotExist(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->trySetObject(['foo', 'bar'], (object) []);
    }

    public function testTrySetObjectFailPathTooShort(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathTooShort::class);
        $box->trySetObject([], (object) []);
    }

    public function testAsObjectAccessors(): void
    {
        $box = ObjectBox::from([
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => [],
            'object' => (object) [],
        ]);
        $this->assertNull($box->asObject('foo'));
        $this->assertNull($box->asObject('null'));
        $this->assertNull($box->asObject('int'));
        $this->assertNull($box->asObject('float'));
        $this->assertNull($box->asObject('bool'));
        $this->assertNull($box->asObject('array'));
        $this->assertJsonStringEqualsJsonString(
            json_encode($box->asObject('object'), JSON_THROW_ON_ERROR),
            json_encode((object) [], JSON_THROW_ON_ERROR)
        );
        $this->assertNull($box->asObject('non_existent'));
    }

    public function testTryAsObjectString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->expectException(MismatchedType::class);
        $box->tryAsObject('foo');
    }

    public function testTryAsObjectNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->expectException(MismatchedType::class);
        $box->tryAsObject('foo');
    }

    public function testTryAsObjectInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->expectException(MismatchedType::class);
        $box->tryAsObject('foo');
    }

    public function testTryAsObjectFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->expectException(MismatchedType::class);
        $box->tryAsObject('foo');
    }

    public function testTryAsObjectBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->expectException(MismatchedType::class);
        $box->tryAsObject('foo');
    }

    public function testTryAsObjectArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->expectException(MismatchedType::class);
        $box->tryAsObject('foo');
    }

    public function testTryAsObjectObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->assertJsonStringEqualsJsonString(
            json_encode($box->tryAsObject('foo'), JSON_THROW_ON_ERROR),
            json_encode((object) [], JSON_THROW_ON_ERROR)
        );
    }

    public function testTryAsObjectNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->tryAsObject('foo');
    }

    public function testIsObjectString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->assertFalse($box->isObject('foo'));
    }

    public function testIsObjectNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->assertFalse($box->isObject('foo'));
    }

    public function testIsObjectInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->assertFalse($box->isObject('foo'));
    }

    public function testIsObjectFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->assertFalse($box->isObject('foo'));
    }

    public function testIsObjectBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->assertFalse($box->isObject('foo'));
    }

    public function testIsObjectArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->assertFalse($box->isObject('foo'));
    }

    public function testIsObjectObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->assertTrue($box->isObject('foo'));
    }

    public function testIsObjectNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->assertFalse($box->isObject('foo'));
    }
}
