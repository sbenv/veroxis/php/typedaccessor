<?php

declare(strict_types=1);

namespace Tests;

use JsonException;
use PHPUnit\Framework\TestCase;
use Veroxis\Typedaccessor\Json;

use function property_exists;

final class JsonTest extends TestCase
{
    public function testDecodeArray(): void
    {
        $json = '["bar"]';
        $data = Json::decode($json);
        $this->assertIsArray($data);
        $this->assertArrayHasKey(0, $data);
        $this->assertSame('bar', $data['0']);
    }

    public function testDecodeObject(): void
    {
        $json = '{"foo":"bar"}';
        $data = Json::decode($json);
        $this->assertIsObject($data);
        $this->assertTrue(property_exists($data, 'foo'));
        $this->assertSame('bar', $data->foo);
    }

    public function testDecodeInt(): void
    {
        $json = 1;
        $this->expectException(JsonException::class);
        Json::decode((string) $json);
    }

    public function testDecodeString(): void
    {
        $json = 'foo';
        $this->expectException(JsonException::class);
        Json::decode($json);
    }

    public function testDecodeFloat(): void
    {
        $json = 3.1415926535;
        $this->expectException(JsonException::class);
        Json::decode((string) $json);
    }

    public function testDecodeTrue(): void
    {
        $json = true;
        $this->expectException(JsonException::class);
        Json::decode((string) $json);
    }

    public function testDecodeFalse(): void
    {
        $json = false;
        $this->expectException(JsonException::class);
        Json::decode((string) $json);
    }

    public function testDecodeNull(): void
    {
        $json = null;
        $this->expectException(JsonException::class);
        Json::decode((string) $json);
    }

    public function testDecodeEmptyString(): void
    {
        $json = '';
        $this->expectException(JsonException::class);
        Json::decode($json);
    }

    public function testDecodeNullString(): void
    {
        $json = "\0";
        $this->expectException(JsonException::class);
        Json::decode($json);
    }

    public function testEncodeEmpty(): void
    {
        $data = [];
        $json = Json::encode($data);
        $this->assertSame('[]', $json);
    }

    public function testEncodeAssociative(): void
    {
        $data = ['foo' => 'bar'];
        $json = Json::encode($data);
        $this->assertJsonStringEqualsJsonString('{"foo":"bar"}', $json);
    }

    public function testEncodeList(): void
    {
        $data = ['foo', 'bar'];
        $json = Json::encode($data);
        $this->assertJsonStringEqualsJsonString('["foo","bar"]', $json);
    }
}
