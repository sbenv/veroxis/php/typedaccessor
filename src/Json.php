<?php

declare(strict_types=1);

namespace Veroxis\Typedaccessor;

use JsonException;

use function json_decode;
use function json_encode;

use const JSON_THROW_ON_ERROR;

class Json
{
    /**
     * @param mixed[]|object $data
     *
     * @throws JsonException
     */
    public static function encode(array|object $data): string
    {
        return json_encode($data, JSON_THROW_ON_ERROR);
    }

    /**
     * @return mixed[]|object
     *
     * @throws JsonException
     */
    public static function decode(string $json): array|object
    {
        $decoded = json_decode($json, false, JSON_THROW_ON_ERROR);
        if (is_array($decoded)) {
            return $decoded;
        }
        if (is_object($decoded)) {
            return $decoded;
        }
        throw new JsonException("json string represents neither `array` nor `object`: `{$json}`");
    }
}
