<?php

declare(strict_types=1);

namespace Veroxis\Typedaccessor\Exceptions;

use Exception;

class MismatchedType extends Exception
{
    /**
     * @return never
     */
    public static function throw(string $expected, mixed $data): void
    {
        throw new self(sprintf(
            'tried to get typed property as `%s` but got `%s`',
            $expected,
            get_debug_type($data)
        ));
    }
}
