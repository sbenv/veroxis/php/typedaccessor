<?php

declare(strict_types=1);

namespace Veroxis\Typedaccessor\Exceptions;

use Exception;

class PathDoesNotExist extends Exception
{
    /**
     * @return never
     */
    public static function throw(string|int ...$allKeys): void
    {
        throw new self(sprintf('path [%s] does not exist', implode(', ', $allKeys)));
    }
}
