<?php

declare(strict_types=1);

namespace Veroxis\Typedaccessor\Exceptions;

use Exception;

class PathTooShort extends Exception
{
    /**
     * @return never
     */
    public static function throw(): void
    {
        throw new self();
    }
}
